-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2019 at 05:11 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forum`
--

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE `forum` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `tgl_terbit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forum`
--

INSERT INTO `forum` (`id`, `judul`, `isi`, `tgl_terbit`) VALUES
(1, 'Forum Diskusi pertama', '<p>Ini Adalah Forum Diskusi Pertama?</p>', '2019-11-04'),
(4, 'Ini Forum Diskusi kedua ya..!', '<p>Forum diskusi ini dibuat supaya keliatan rame</p>', '2019-11-04'),
(6, 'Apakah bumi itu datar atau bulat?', '<p>saya masih bingung gan, bumi itu bulat atau datar karena kedua teori memiliki argumen yang sama-sama kuat</p>', '2019-11-04'),
(7, 'Apakah ada yang mengerti maksud parse errors?', '<p>Saya menemukan keternangan error pada php yaitu <strong>Parse Errors (syntax errors), </strong>ada yang mengerti maksudnya?</p>', '2019-11-04'),
(8, 'Rekomendasi Film', '<p>Butuh refreshing nihh gan, ada yang punya rekomendasi film yang bagus?</p>', '2019-11-04'),
(9, 'Cek Waktu', '<p>Berhasil?</p>', '2019-11-04');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` int(11) NOT NULL,
  `forum` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `isi` text NOT NULL,
  `tgl_terbit` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id`, `forum`, `nama`, `isi`, `tgl_terbit`) VALUES
(1, 1, 'Ahmad', 'Ini komentar di Forum Diskusi Pertama', '2019-11-02'),
(2, 1, 'Qonit', 'Ini juga komentar di Forum Diskusi pertama bro...', '2019-11-02'),
(6, 4, 'Me', 'Setuju, Forum diskusi ini memang perlu dibuat ramai...', '2019-11-03'),
(7, 6, 'No_Comman', '<p>Udh bro gk usah dipikirin, gak akan ditanya diakhirat</p>', '2019-11-03'),
(8, 9, 'Test', '<p>Apakah waktu pada komentar berhasil ke post?</p>', '2019-11-04');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `asal` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `asal`) VALUES
(1, 'admin', '$2y$10$Ca4NaciqzO8PJzfxzRiUJeYZXRxcuyK5MPX.9V845YlM/D0dba4IW', 'admin@gmail.com', 'Indonesia'),
(2, 'qonit', '$2y$10$jhKZ/L3rL55sQAiwP8i4YezFJiQXSLdl74xhrcNMVteBbzEvKIsqK', 'santrisiber.qonit@gmail.com', 'Bandung');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forum`
--
ALTER TABLE `forum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forum`
--
ALTER TABLE `forum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
