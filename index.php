<?php  
session_start();

if( !isset($_SESSION["login"]) ) {
	header("Location: login.php");
	exit;
}

require 'fungsi/fungsi_user.php';
require 'fungsi/config.php';

$data = tampilForum();
// tombol cari ditekan
if( isset($_POST["cari"]) ) {
	$data = cari($_POST["keyword"]);
}

?>



<!DOCTYPE html>
<html>
<head>
	<title>Forum Diskusi</title>
	<link rel="stylesheet" type="text/css" href="assets/css/index.css">
</head>
<body>

	<div class="header">
		<div >
			<a href="" id="font-forum">F-O-R-U-Me</a>
		</div>

		<div class="search">
			<form action="" method="post">
				<input type="text" id="searchbox" name="keyword" placeholder="Cari Topik..." autocomplete="off">
				<button type="submit" id="buttonCari" name="cari">Cari!</button>
			</form>
		</div>

		<div>
			<img src="assets/img/user.jpg" id="img-User">
		</div>

		<div>
			<a href="" id="font-User"><?= $_SESSION["username"]; ?></a>
		</div>

		<div>
			<a href="logout.php" id="logout">Logout</a>
		</div>
	</div>

	<br><br>

	<div class="container">
		<div class="title">
			<h1>Daftar Forum Diskusi</h1>
			<a href="inputForum.php">
				<button>
					Tambah Forum
				</button>
			</a>
		</div>


		<?php $i = 1; ?>
		<?php foreach ($data as $row) : ?>
		<div class="box">
			<?= $i ?>
			<hr>
			<h3>
				<a href="detail.php?id=<?= $row['id'] ?>">
						<?= $row['judul'] ?>
				</a>
			</h3>
			<a href="editForum.php?id=<?= $row['id'] ?>" id="edit">
				Edit
			</a>
			|
			<a href="hapusForum.php?id=<?= $row['id'] ?>" onclick="return confirm('Apakah Anda Yakin ingin Menghapus Forum?')" id="hapus">
				Hapus
			</a>
			<br><br>
			<?= jumlahKomentar($row['id']); ?> Komentar
		</div>
		<?php $i++; ?>
		<?php endforeach ?>
	</div>
	

</body>
</html>